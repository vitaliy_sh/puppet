# fixup permissions on sudo
class sudo {
    file { "/etc/sudoers":
        owner => root,
        group => root,
        mode => 440,
    }
}

node default {
        Package { ensure => "installed" }
        package { "screen": }
        package { "sudo": }
        package { "wget": }
        package { "openvpn": }
        package { "vim": }
        package { "tcpdump": }
        include sudo
}
