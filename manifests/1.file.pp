file { '/root/.ssh/authorized_keys':
	path => '/root/.ssh/authorized_keys',
	source => '/etc/puppet/authorized_keys',
	owner => 'root',
	group => 'root',
	mode => 600,
	ensure => file,
}

file { '/home/vvs/.ssh': 
	owner => 'vvs',
	group => 'vvs',
	ensure => directory,
	mode => 644,
	before => File['/home/vvs/.ssh/authorized_keys'],
}

file { '/home/vvs/.ssh/authorized_keys':
        path => '/home/vvs/.ssh/authorized_keys',
        source => '/etc/puppet/authorized_keys',
        owner => 'vvs',
        group => 'vvs',
        mode => 600,
        ensure => file,
}

user { 'vvs':
	ensure => present,
	home => '/home/vvs',
	shell => '/bin/bash',
	managehome => true,
	before => File['/home/vvs/.ssh/authorized_keys'],
}

package { 'openssh-server':
	ensure => present,
	before => File['/etc/ssh/sshd_config'],
}

file { '/etc/ssh/sshd_config':
	ensure => file,
	mode => 600,
	source => '/etc/puppet/sshd_config',
}

service { 'ssh':
	ensure => running,
	enable => true,
	hasrestart => true,
	hasstatus => true,
	subscribe => File['/etc/ssh/sshd_config'],
}
